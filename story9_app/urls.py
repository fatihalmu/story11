from django.urls import path
from .views import bukurekomen_views,subscribe_views,validasi_email,subscribe_click,login_views,token_views,clear_session_views	#url for app
urlpatterns = [
	path('',bukurekomen_views,name='bukurekomendasi'),
	path('validasi/<email>/',validasi_email,name='validasi'),
	path('subscribe/',subscribe_views,name='subscription'),
	path('subscribeclick/',subscribe_click,name='subsclick'),
	path('log-in/',login_views,name='loginpage'),
	path('token/',token_views,name='token'),
	path('clearsession/',clear_session_views,name='clearsession')



	]
