import json
import requests as api_request
from django.shortcuts import render
from django.shortcuts import reverse
from django.http import HttpResponse,HttpResponseRedirect
from .forms import subscription
from .models import Subscribe_model

BOOK_API = 'https://www.googleapis.com/books/v1/volumes?q='

response = {}

# Create your views here.
def token_views(request):
	link ='https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='
	token = request.GET.get('token')
	data = api_request.get(link+token).json()
	print(data)
	request.session['token']= token
	request.session['name'] =  data['name']
	request.session['email'] =  data['email']
	request.session['urlgambar']= data['picture']
	return HttpResponse(json.dumps({'login':'success'}))

def login_views(request):
	data_lempar={}
	if 'name' in request.session.keys():

		data_lempar['name'] = request.session['name']
		data_lempar['ada'] = True

	if 'token' in request.session.keys():
		data_lempar['token'] = request.session['token']
	if 'email' in request.session.keys():
		data_lempar['email'] = request.session['email']
	if 'urlgambar' in request.session.keys():
		data_lempar['urlgambar'] = request.session['urlgambar']
	print(data_lempar['ada'])
	print(data_lempar['name'])
	return render(request,'login.html',data_lempar)
def clear_session_views():
	request.session.flush()
	return HttpResponse(json.dumps({'login':'success'}))

def bukurekomen_views(request):
	books = json.dumps(BOOK_API)
	response['books'] = books
	return render(request,'bukumenarik.html',response)

def subscribe_views(request):
	jumlah = Subscribe_model.objects.all();
	if request.method == "POST":
		form = subscription(request.POST)
		if form.is_valid():
			agendabaru = form.save()
			return HttpResponseRedirect(reverse('subscription'))
	else:
		form = subscription()

	return render(request, 'subscribe.html', {'form': form,'jumlah':jumlah})

def validasi_email(request,email= None):
	email_model= Subscribe_model.objects.filter(email__iexact=email).exists()
	data = {'ada': email_model}
	if data['ada']:
		data['message'] = 'Email tersebut sudah pernah didaftarkan sebelumnya, silahkan daftar dengan email lain.'
	else:
		data['message'] = 'Email belum terdaftar, silahkan daftar dengan email ini.'
	json_data = json.dumps(data)
	return HttpResponse(json_data, content_type='application/json')
def subscribe_click (request):
	if(request.method== 'POST'):
		nama = request.POST['nama']
		email = request.POST['email']
		password = request.POST['password']
		Subscribe_model.objects.create(nama=nama,email=email,password=password)
		return HttpResponse('')
	
