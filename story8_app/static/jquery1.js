$(document).ready(function(){
		function showKonten(){
			$("#loader").css("display","none");
			$("#badan-konten").css("display","block");
		};
		window.setTimeout(showKonten,2000);

		//untuk accordion
		var panels = $("#accordionku .panel").hide();
		$("#accordionku .accordion").click(function(){
			
			var ini = $(this);
			panels.slideUp();
			ini.next().slideDown();
		});
		//kalau button red di tekan
		$("#button-red").click(function(){

			$(".accordion").css({"background-color":"#f41111","color":"black"});
			$("body").css("background-color","red");
			
			$(".accordion").hover(function(){
				$(this).css("background-color","green");},
				function(){
					$(this).css("background-color","#f41111");

			});
		});

		//kalau button green ditekan
		$("#button-green").click(function(){
			$(".accordion").css({"background-color":"green","color":"black"});
			$("body").css("background-color","green");

			$(".accordion").hover(function(){
				$(this).css("background-color","#f41111");
			},function(){
				$(this).css("background-color","green");
			});
		});

		//kalau button normal ditekan
		$("#button-normal").click(function(){
			$(".accordion").css({"background-color":"white","color":"black"});
			$("body").css("background-color","white");

			$(".accordion").hover(function(){
				$(this).css("background-color","blue");
			},function(){
				$(this).css("background-color","white");
			});
		});

	});